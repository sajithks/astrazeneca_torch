
--data reading
require 'hdf5'
require 'torch'
require 'nn'
require 'optim'
require 'cunn'
require 'cudnn'
require 'nngraph'
nt=require 'net-toolkit'
--model cleaning

function zeroDataSize(data)
  if type(data) == 'table' then
    for i = 1, #data do
      data[i] = zeroDataSize(data[i])
    end
  elseif type(data) == 'userdata' then
    data = torch.Tensor():typeAs(data)
  end
  return data
end

-- Resize the output, gradInput, etc temporary tensors to zero (so that the
-- on disk size is smaller)
function cleanupModel(node)
  if node.output ~= nil then
    node.output = zeroDataSize(node.output)
  end
  if node.gradInput ~= nil then
    node.gradInput = zeroDataSize(node.gradInput)
  end
  if node.finput ~= nil then
    node.finput = zeroDataSize(node.finput)
  end
  -- Recurse on nodes with 'modules'
  if (node.modules ~= nil) then
    if (type(node.modules) == 'table') then
      for i = 1, #node.modules do
        local child = node.modules[i]
        cleanupModel(child)
      end
    end
  end
  collectgarbage()
  end
---
local Conv = cudnn.SpatialConvolution
local ReLU = cudnn.ReLU
local Max = nn.SpatialMaxPooling
local Sbatchnorm = nn.SpatialBatchNormalization
local Fconv = cudnn.SpatialFullConvolution
-- create conv-batchnorm-relu layer

local function createConvlayer(nInputPlane, nOutputPlane, kernsize, stride, pad)
  clayer = nn.Sequential()
  clayer:add(Conv(nInputPlane, nOutputPlane, kernsize, kernsize, stride, stride, pad, pad))
  clayer:add(Sbatchnorm(nOutputPlane))
  clayer:add(ReLU())
  return clayer
end

-- create conv-batchnorm-relu-maxpool layer

local function createConvPoollayer(nInputPlane, nOutputPlane, kernsize, stride, pad)
  clayer = nn.Sequential()
  clayer:add(Conv(nInputPlane, nOutputPlane, kernsize, kernsize, stride, stride, pad, pad))
  clayer:add(Sbatchnorm(nOutputPlane))
  clayer:add(ReLU())
  clayer:add(Max(2, 2, 2, 2))
  return clayer
end

--residual layer creation

local function createReslayer(input, nInputPlane, nOutputPlane)
  -- input = nn.Identity()()
  layer1 = createConvlayer(nInputPlane, 16, 1, 1, 0)(input)
  layer2 = createConvlayer(16, 16, 3, 1, 1)(layer1)
  layer3 = createConvlayer(16, nOutputPlane, 1, 1, 0)(layer2)
  elemwiseadd = nn.CAddTable()({input, layer3})
  snorm = Sbatchnorm(nOutputPlane)(elemwiseadd)
  -- resmod = nn.gModule({input},{snorm})
  return snorm
end

--residual layer creation2 dynamic size
local function createReslayer2(input, nInputPlane, nOutputPlane)
  -- input = nn.Identity()()
  layer1 = createConvlayer(nInputPlane, nOutputPlane, 1, 1, 0)(input)
  layer2 = createConvlayer(nOutputPlane, nOutputPlane, 3, 1, 1)(layer1)
  layer3 = createConvlayer(nOutputPlane, nOutputPlane, 1, 1, 0)(layer2)
  elemwiseadd = nn.CAddTable()({input, layer3})
  snorm = Sbatchnorm(nOutputPlane)(elemwiseadd)
  -- resmod = nn.gModule({input},{snorm})
  return snorm
end


-- deconv layer
local function createDeconvlayer(input, nInputPlane, nOutputPlane)
  layer1 = Fconv(nInputPlane, nOutputPlane, 2, 2, 2, 2, 0, 0, 0, 0)(input)
  layer2 = Sbatchnorm(nOutputPlane)(layer1)
  layer3 = ReLU()(layer2)
  layer4 = createConvlayer(nOutputPlane,nOutputPlane, 3, 1, 1)(layer3)
  layer5 = createConvlayer(nOutputPlane,nOutputPlane, 3, 1, 1)(layer4)
  return layer5
end

-- myFile = hdf5.open('/Users/sajithks/Documents/deeptraing/data_astrazenica/groundtruth/smalldataset/az0.h5', 'r')
-- data = myFile:read('/data'):all()
-- label = myFile:read('/label'):all()
-- labelwt = myFile:read('/labelwt'):all()
-- bound = myFile:read('/bound'):all()
--trainfile = "/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/smalldataset/train.txt"
trainfile = "/mnt/harddisk/sajith/astrazeneca_data/dataset/data3/dataset_large_6k_wt3_32bit/train.txt"

batchsize = 1--30

--outfolder = "/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/"
outfolder = "/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/savetest/"

--create model
convsize1 = 8--32--8
convsize2 = 16--64--16
resplane = 64--256--64

input = nn.Identity()()
layer1 = createConvPoollayer(1, convsize1, 3, 1, 1)(input)
layer2 = createConvPoollayer(convsize1, convsize2, 3, 1, 1)(layer1)
layer3 = createConvlayer(convsize2, resplane, 3, 1, 1)(layer2)
layer4 = createReslayer2(layer3, resplane, resplane)
layer5 = createReslayer2(layer4, resplane, resplane)
layer6 = createReslayer2(layer5, resplane, resplane)
layer7 = createReslayer2(layer6, resplane, resplane)

--long skip paths
layer8 = createReslayer2(layer7, resplane, resplane)
eadd8 = nn.CAddTable()({layer8, layer6})
sn8 = Sbatchnorm(resplane)(eadd8)

layer9 = createReslayer2(sn8, resplane, resplane)
eadd9 = nn.CAddTable()({layer9, layer7})
sn9 = Sbatchnorm(resplane)(eadd9)

layer10 = createReslayer2(sn9, resplane, resplane)
eadd10 = nn.CAddTable()({layer10, layer6})
sn10 = Sbatchnorm(resplane)(eadd10)

layer11 = createReslayer2(sn10, resplane, resplane)
eadd11 = nn.CAddTable()({layer11, layer5})
sn11 = Sbatchnorm(resplane)(eadd11)

layer12 = createReslayer2(sn11, resplane, resplane)
eadd12 = nn.CAddTable()({layer12, layer4})
sn12 = Sbatchnorm(resplane)(eadd12)

layer13 = createDeconvlayer(sn12, resplane, convsize2)
eadd13 = nn.CAddTable()({layer13, layer2})
sn13 = Sbatchnorm(convsize2)(eadd13)

layer14 = createDeconvlayer(sn13, convsize2, convsize1)
eadd14 = nn.CAddTable()({layer14, layer1})
sn14 = Sbatchnorm(convsize1)(eadd14)

layer15 = createConvlayer(convsize1, 2, 3, 1, 1)(sn14)

gmod = nn.gModule({input}, {layer15})
model = gmod

-- data = data:double()
-- label = label:double()
-- network model creation
-- model = nn.Sequential()
--
-- --layer 1
-- model:add(nn.SpatialConvolution(1, 4, 3, 3, 1, 1, 1, 1))
-- model:add(nn.SpatialBatchNormalization(4))
-- model:add(nn.ReLU())
--model:add(nn.Reshape(1,2*480*480))
--model:add(nn.View(2*3*3))
--layer 2
-- model:add(nn.SpatialConvolutionMM(4, 4, 3, 3, 1, 1, 1, 1))
-- model:add(nn.SpatialBatchNormalization(4))
-- model:add(nn.ReLU())

--layer 3
-- model:add(nn.SpatialConvolutionMM(4, 2, 3, 3, 1, 1, 1, 1))
-- model:add(nn.SpatialBatchNormalization(2))
-- model:add(nn.ReLU())

-- print(model)

-- loss function

--model:add(nn.LogSoftMax())
--criterion = nn.SpatialClassNLLCriterion()
--criterion = nn.CrossEntropyCriterion();
--weighting each label differently

labelweight = torch.Tensor(2)
labelweight[1] = 0.5
labelweight[2] = 1


criterion = cudnn.SpatialCrossEntropyCriterion(labelweight)
--print(criterion)

--- training

model:cuda()
criterion:cuda()

trainLogger = optim.Logger(paths.concat(outfolder, 'save/train.log'))

learningRate = 1e-2
weightDecay = 0
momentum = 0.9

optimState = {
      learningRate = learningRate,
      weightDecay = weightDecay,
      momentum = momentum,
      learningRateDecay = 1e-7
   }
   optimMethod = optim.rmsprop
  --  optimMethod = optim.sgd

-- set to training mode

-- model:training()

-- set gradients to zero

-- gradParameters:zero()

--for ii = 1, data:size()[1] do
--  output = model:forward(data[i])
--  err = criterion:forward(output, targets[i])
--  print(err)

--end

--indata = nn.utils.addSingletonDimension(data[1])
-- parameters,gradParameters = model:getParameters()

iterationcounter = 0
if model then
   parameters,gradParameters = model:getParameters()
end
function train()
  -- to do read multiple files and read parts of files for memory constraints
  datafile = io.open(trainfile,"r")
  -- epoch tracker
  epoch = epoch or 1

  filenumber = 1
  for filename in datafile:lines() do

    myFile = hdf5.open(filename, 'r')
    data = myFile:read('/data'):all()
    label = myFile:read('/label'):all()

    -- data = data:double()
    -- label = label:double()

    --data = data:cuda()
    --label = label:cuda()


    -- set model for training
    model:training()

    -- no. of samples
    datasize = data:size(1)

    --shuffle data every epoch
    shuffle = torch.randperm(datasize)

    print(" Training epoch- " ..epoch)
    local time = sys.clock()

    --pass through the entire dataset
    for tt = 1, datasize, batchsize do
      --xlua.progress(tt, datasize)

      --create mini batchsize
      dataminibat = {}
      labelminibat = {}
      for ii = tt, math.min(tt + batchsize-1, datasize) do
        datasingle = nn.utils.addSingletonDimension(data[shuffle[ii]])
        labelsingle = label[shuffle[ii]]
        -- transfer to gpu
        datasingle = datasingle:cuda()
        labelsingle = labelsingle:cuda()
        table.insert(dataminibat, datasingle)
        table.insert(labelminibat, labelsingle)
      end

      -- create closure to evaluate forward and backward passes

      local feval = function(x)

        --get new parameters
        if x ~= parameters then
          parameters:copy(x)
        end

        --reset gradients
        gradParameters:zero()

        -- f is the avergage of all criterions(error)
        local f = 0

        -- evaluate function on the mini batch
        for mb = 1, #dataminibat do

          --estimate f (error)
          output = model:forward(dataminibat[mb])
          err = criterion:forward(output, labelminibat[mb])
          f = f + err

          --estimate df/dW (gradients)
          df_do = criterion:backward(output, labelminibat[mb])
          model:backward(dataminibat[mb], df_do)
        end

        -- normalize gradients and f(x) (average error over mini-batch)
        gradParameters:div(#dataminibat)
        f = f/#dataminibat

        --print(iterationcounter)
        --print("errror "..f.."\n")
        iterationcounter = iterationcounter +1
        --trainLogger:add{['iteration'] = iterationcounter}
        trainLogger:add{['iteration'] = iterationcounter, ['error'] = f}

        -- return error and gradient for optimization
        return f, gradParameters
      end

      -- optimize on current mini-batch usign optimizer
      if optimMethod == optim.asgd then
        _,_,average = optimMethod(feval, parameters, optimState)
      else
        optimMethod(feval, parameters, optimState)
      end

    end

    -- time taken
    time = sys.clock() - time
    time = time / #dataminibat
    print("\n==> time to learn 1 sample = " .. (time*1000) .. 'ms')
    print("filenumber "..filenumber)
    filenumber = filenumber +1


  end
    epoch = epoch + 1
  --local filename = paths.concat(outfolder, 'save/model.net')
  --os.execute('mkdir -p ' .. sys.dirname(filename))
    --print('==> saving model to '..filename)
    --torch.save(filename, model)

  end



savnum = 50--500
for count =1,100 do
  train()
  -- savename =  'save/modelnet_'..count.. '.t7'
  -- filename = paths.concat(outfolder,savename)
  -- os.execute('mkdir -p ' .. sys.dirname(filename))
  -- print('==> saving model to '..filename)
  -- lightModel = model:clone()
--  lightModel = model:clone()
--  lightModel:share(model,"bias")
  -- for nodecount=2,lightModel:size() do
  --   print(nodecount)
  --   innercount = lightModel:get(nodecount):size()
  --   for innernodecont = 1, innercount do
  --     cleanupModel(lightModel:get(nodecount):get(innernodecont))
  --   end
  -- end

  -- torch.save(filename, model)
  if count%savnum ==0
   then
     savename =  'save/modelnet8_'..count.. '.t7'
     filename = paths.concat(outfolder,savename)
     os.execute('mkdir -p ' .. sys.dirname(filename))
     print('==> saving model to '..filename)

     torch.save(filename, model)
  end


end
-- w, dw = nt.saveNet(filename, model)
