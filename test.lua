----------------------------------------------------------------------
-- This script implements a test procedure, to report accuracy
-- on the test data. Nothing fancy here...
--
-- Clement Farabet
----------------------------------------------------------------------
require 'hdf5'

require 'torch'   -- torch
require 'xlua'    -- xlua provides useful tools, like progress bars
require 'optim'   -- an optimization package, for online and batch methods
require 'image'
cv = require 'cv'
require 'cv.imgcodecs'
require 'nn'
nt=require 'net-toolkit'
require 'nngraph'
require 'cunn'
require 'cudnn'
----------------------------------------------------------------------
print '==> defining test procedure'

-- test function
-- function test()
 -- local vars
 -- modelname = '/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/save/modelnet_500.t7'
 -- model, w, dw = nt.loadNet(modelname)
 --model = torch.load('/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/savetest/save/modelnet32_50.t7')
 model = torch.load('/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/savetest/save/modelnet32_1.t7')
 -- model = torch.load(modelname)

 -- model, w, dw = nt.loadNet(fileName, format)
model = model:cuda()

-- local time = sys.clock()

 -- averaged param use?
-- if average then
--   cachedparams = parameters:clone()
--   parameters:copy(average)
-- end

 -- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
model:evaluate()
--read from hdf5 file

--img = '/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/testimg.png'

--myFile = hdf5.open('/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/smalldataset/az0.h5', 'r')
--data = myFile:read('/data'):all()
--label = myFile:read('/label'):all()
--img = data[1][1]


--outname = '/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/probmap/out.png'
outname = '/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/savetest/save/out.png'
input = cv.imread{'/mnt/harddisk/sajith/astrazeneca_data/torch_models/sampletest/model/testimg.png',-1}
-- input = cv.imread{img,-1}
input = input:double()
input = input-input:min()
input = input/input:max()
input = input-input:mean()


input = nn.utils.addSingletonDimension(input)
input = nn.utils.addSingletonDimension(input)
input = input:cuda()
pred = model:forward(input)

image.save(outname,nn.SoftMax():forward(pred:double())[1][2] )
--image.save(outname, pred[1][1])
   -- test over test data
--    print('==> testing on test set:')
--    for t = 1,testData:size() do
--       -- disp progress
--       xlua.progress(t, testData:size())
--
--       -- get new sample
--       local input = testData.data[t]
--       if opt.type == 'double' then input = input:double()
--       elseif opt.type == 'cuda' then input = input:cuda() end
--       local target = testData.labels[t]
--
--       -- test sample
--       local pred = model:forward(input)
--       confusion:add(pred, target)
--    end
--
--    -- timing
--    time = sys.clock() - time
--    time = time / testData:size()
--    print("\n==> time to test 1 sample = " .. (time*1000) .. 'ms')
--
--    -- print confusion matrix
--    print(confusion)
--
--    -- update log/plot
--    testLogger:add{['% mean class accuracy (test set)'] = confusion.totalValid * 100}
--    if opt.plot then
--       testLogger:style{['% mean class accuracy (test set)'] = '-'}
--       testLogger:plot()
--    end
--
--    -- averaged param use?
--    if average then
--       -- restore parameters
--       parameters:copy(cachedparams)
--    end
--
--    -- next iteration:
--    confusion:zero()
-- end
